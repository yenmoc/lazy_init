﻿using System;
using System.Collections;
using System.Threading;
using UnityEngine;

public class LazyInit : MonoBehaviour
{
    public void Start()
    {
        var lazyObj = new Lazy<object>(LoadData);

        Debug.Log(lazyObj); // (1)
        Debug.Log(lazyObj.Value); // (2) (3)


//        CharacterData lazyObj = null;
//        Debug.Log("Data has not been loaded.");
//
//        LazyInitializer.EnsureInitialized(ref lazyObj, LoadData);
//        Debug.Log(lazyObj.name);
    }

    CharacterData LoadData()
    {
        Debug.Log("Start loading data.");
        for (int i = 0; i < 1000; i++)
        {
            Debug.Log(i);
        }
        return new CharacterData(1, "Lauza");
    }


    public class CharacterData
    {
        public int id;
        public string name;

        public CharacterData(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}